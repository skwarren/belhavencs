from flask import Flask
from flask import url_for
from flask import render_template

app = Flask(__name__)

@app.route('/portfolio')
def find_porfolio():
    return render_template('portfolio.html')

@app.route('/')
def home_page():
    return render_template('homepage.html')
