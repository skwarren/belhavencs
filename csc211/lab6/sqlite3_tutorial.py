import sqlite3
import hashlib
import csv

try:
    def encrypt_password(password):
        encrypted_pass = hashlib.sha256(password.encode('utf-8')).hexdigest()
        return encrypted_pass

    db = sqlite3.connect('users.db')
    db.create_function('encrypt', 1, encrypt_password)
    #creating cursor
    cursor = db.cursor()
    #creates table to be committed to db
    cursor.execute('''
    CREATE TABLE IF NOT EXISTS users(id INTEGER PRIMARY KEY, name TEXT,
                    phone TEXT, email TEXT UNIQUE, password TEXT, created_at timestamp
                        DATETIME DEFAULT CURRENT_TIMESTAMP)

                        ''')
    with open('lab6-names.csv') as csvfile:
        dr = csv.DictReader(csvfile)
        to_db = [(i['name'], i['phone'], i['email'], i['password']) for i in dr]

    cursor.executemany('''INSERT INTO users(name, phone, email, password) VALUES (?, ?, ?, encrypt(?));''', to_db)

    #commits to db
    db.commit()
    find_number = cursor.execute('''SELECT name FROM users
                                    WHERE phone LIKE "9%" ''')
    count = 0
    for row in find_number:
        print(row)
        count += 1
    print('count is', count)
#If exception, do a rollback and tell us what error is
except Exception as e:
    db.rollback()
    raise e
finally:
    db.close()
