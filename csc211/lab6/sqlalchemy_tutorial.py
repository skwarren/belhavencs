
from sqlalchemy import Column, Integer, String, create_engine, DateTime
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker


engine = create_engine('sqlite:///users.db', echo=True)
Base = declarative_base()

class User(Base):
    __tablename__ = 'users'

    id = Column(Integer, primary_key=True)
    name = Column(String)
    email = Column(String)
    phone = Column(String)
    password = Column(String)
    created_at = Column(DateTime)

    def __repr__(self):
        return "<User(name='%s', email='%s', phone='%s', password='%s', create_at='%d')>"(self.name, self.email, self.password, self.created_at)
#User.__table__
#Base.metadata.create_all(engine)
Session = sessionmaker()
Session.configure(bind=engine)
session = Session()

#prints name who has phone number starting with 9
count = 0
for person in session.query(User.name).filter(User.phone.like("9%")):
    print(person)
    count +=1
print(count)
