import datetime

import sqlalchemy
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Columns, Integer, String, DateTime
from sqlalchemy.orm import sessionmaker

engine = create_engine('sqlite:///users.db', echo=True)

Session = sessionmaker(bind=engine)
Base = declarative_base()

def get_session():
    return Session()

def user_query():
    return get_session().query(User)

class User(Base):
    __tablename__ = 'users'

    id = Column(Interger, primary_key = True)
    name = Column(String)
    phone = Column(String)
    email = Column(String, Unique)
    password = Column(String)
    created_at = Column(DateTime, default=datetime.datetime.now)
